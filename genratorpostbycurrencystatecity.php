<?php
/*
Plugin Name:Generador de Post por Paices, Estados y Ciudades
Plugin URI: https://gitlab.com/franciscoblancojn/genratorpostbycurrencystatecity
Description: Plugin for Wordpress, para generar post por Paices, Estados y Ciudades
Author: Francisco Blanco
Version: 1.3.0
Author URI: https://franciscoblanco.vercel.app/
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/franciscoblancojn/genratorpostbycurrencystatecity',
	__FILE__,
	'genratorpostbycurrencystatecity'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch('master');

function GPPEC_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}

define("GPPEC_LOG",true);
define("GPPEC_PATH",plugin_dir_path(__FILE__));
define("GPPEC_URL",plugin_dir_url(__FILE__));

require_once GPPEC_PATH . "src/_index.php";