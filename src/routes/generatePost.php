<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

$data = json_decode(file_get_contents('php://input'), true);
header('Content-Type: application/json; charset=utf-8');
if(isset($data)){
    $_POST = $data;
}

$variation = $_POST["variation"];

if(empty($variation)){
    GPPEC_errorApi("variation is empty");
}
$country = $variation["country"];
if(empty($country)){
    GPPEC_errorApi("country is empty");
}
$countryID = $country["id"];
if(empty($countryID)){
    GPPEC_errorApi("country id is empty");
}
$countryText = $country["text"];
if(empty($countryText)){
    GPPEC_errorApi("country text is empty");
}


$state = $variation["state"];
if(empty($state)){
    GPPEC_errorApi("state is empty");
}
$stateID = $state["id"];
if(empty($stateID)){
    GPPEC_errorApi("state id is empty");
}
$stateText = $state["text"];
if(empty($stateText)){
    GPPEC_errorApi("state text is empty");
}


$city = $variation["city"];
if(empty($city)){
    GPPEC_errorApi("city is empty");
}
$cityID = $city["id"];
if(empty($cityID)){
    GPPEC_errorApi("city id is empty");
}
$cityText = $city["text"];
if(empty($cityText)){
    GPPEC_errorApi("city text is empty");
}

$extraTitle = $variation["extraTitle"];
if(empty($extraTitle)){
    GPPEC_errorApi("extraTitle is empty");
}

$address = $variation["address"];
if(empty($address)){
    GPPEC_errorApi("address is empty");
}

$post = $variation["post"];

if(empty($post)){
    GPPEC_errorApi("post is empty");
}

$post_id = $post["ID"];

if(empty($post_id)){
    GPPEC_errorApi("post_id is empty");
}
$post = get_post( $post_id );

if(!$post){
    GPPEC_errorApi("post_id invalid"); 
}
$post->post_title = $post->post_title . " " . $extraTitle;
$post->post_name = $post->post_name . " " . $extraTitle;

if($_POST["notDuplicate"]){
    $exitsPost = get_page_by_title($post->post_title,"OBJECT",$post->post_type);

    if($exitsPost != null){
        GPPEC_errorApi("post exits"); 
    }
}

unset($post->ID);
unset($post->guid);
unset($post->post_modified);
unset($post->post_modified_gmt);
unset($post->post_date);
unset($post->post_date_gmt);
unset($post->to_ping);
unset($post->pinged);
unset($post->ping_status);

$new_post_id = wp_insert_post($post);

if(is_wp_error($new_post_id)){
    GPPEC_errorApi($new_post_id); 
}
$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
if (count($post_meta_infos)!=0) {
    $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
    foreach ($post_meta_infos as $meta_info) {
        $meta_key = $meta_info->meta_key;
        if( $meta_key == '_wp_old_slug' ) continue;
        $meta_value = addslashes($meta_info->meta_value);
        $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
    }
    $sql_query.= implode(" UNION ALL ", $sql_query_sel);
    $wpdb->query($sql_query);
}


update_post_meta($new_post_id,"GPPEC_country",$countryText);
update_post_meta($new_post_id,"GPPEC_country_id",$countryID);

update_post_meta($new_post_id,"GPPEC_state",$stateText);
update_post_meta($new_post_id,"GPPEC_state_id",$stateID);

update_post_meta($new_post_id,"GPPEC_city",$cityText);
update_post_meta($new_post_id,"GPPEC_city_id",$cityID);
update_post_meta($new_post_id,"GPPEC_post","yes");

update_post_meta($new_post_id,"GPPEC_extraTitle",$extraTitle);


echo json_encode(array(
    "status"    => 200,
    "type"      => "ok",
    "messaje"   => "Successfully created variation",
));
exit;