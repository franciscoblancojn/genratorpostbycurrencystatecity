<?php
function GPPEC_getCustomField($atts, $content = null )
{
    $a = shortcode_atts( array(
        'customField' => 'GPPEC_extraTitle',
    ), $atts );
    $post_id = get_the_ID();
    return get_post_meta($post_id, $a['customField'] ,true);
}


add_shortcode('GPPEC_getCustomField', 'GPPEC_getCustomField');