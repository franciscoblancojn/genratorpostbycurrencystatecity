const GPPEC_getCountry = () => [
  {
    id: 144,
    text: "Afganistán",
  },
  {
    id: 114,
    text: "Albania",
  },
  {
    id: 18,
    text: "Alemania",
  },
  {
    id: 98,
    text: "Algeria",
  },
  {
    id: 145,
    text: "Andorra",
  },
  {
    id: 119,
    text: "Angola",
  },
  {
    id: 4,
    text: "Anguilla",
  },
  {
    id: 147,
    text: "Antigua y Barbuda",
  },
  {
    id: 207,
    text: "Antillas Holandesas",
  },
  {
    id: 91,
    text: "Arabia Saudita",
  },
  {
    id: 5,
    text: "Argentina",
  },
  {
    id: 6,
    text: "Armenia",
  },
  {
    id: 142,
    text: "Aruba",
  },
  {
    id: 1,
    text: "Australia",
  },
  {
    id: 2,
    text: "Austria",
  },
  {
    id: 3,
    text: "Azerbaiyán",
  },
  {
    id: 80,
    text: "Bahamas",
  },
  {
    id: 127,
    text: "Bahrein",
  },
  {
    id: 149,
    text: "Bangladesh",
  },
  {
    id: 128,
    text: "Barbados",
  },
  {
    id: 9,
    text: "Bélgica",
  },
  {
    id: 8,
    text: "Belice",
  },
  {
    id: 151,
    text: "Benín",
  },
  {
    id: 10,
    text: "Bermudas",
  },
  {
    id: 7,
    text: "Bielorrusia",
  },
  {
    id: 123,
    text: "Bolivia",
  },
  {
    id: 79,
    text: "Bosnia y Herzegovina",
  },
  {
    id: 100,
    text: "Botsuana",
  },
  {
    id: 12,
    text: "Brasil",
  },
  {
    id: 155,
    text: "Brunéi",
  },
  {
    id: 11,
    text: "Bulgaria",
  },
  {
    id: 156,
    text: "Burkina Faso",
  },
  {
    id: 157,
    text: "Burundi",
  },
  {
    id: 152,
    text: "Bután",
  },
  {
    id: 159,
    text: "Cabo Verde",
  },
  {
    id: 158,
    text: "Camboya",
  },
  {
    id: 31,
    text: "Camerún",
  },
  {
    id: 32,
    text: "Canadá",
  },
  {
    id: 130,
    text: "Chad",
  },
  {
    id: 81,
    text: "Chile",
  },
  {
    id: 35,
    text: "China",
  },
  {
    id: 33,
    text: "Chipre",
  },
  {
    id: 82,
    text: "Colombia",
  },
  {
    id: 164,
    text: "Comores",
  },
  {
    id: 112,
    text: "Congo (Brazzaville)",
  },
  {
    id: 165,
    text: "Congo (Kinshasa)",
  },
  {
    id: 166,
    text: "Cook, Islas",
  },
  {
    id: 84,
    text: "Corea del Norte",
  },
  {
    id: 69,
    text: "Corea del Sur",
  },
  {
    id: 168,
    text: "Costa de Marfil",
  },
  {
    id: 36,
    text: "Costa Rica",
  },
  {
    id: 71,
    text: "Croacia",
  },
  {
    id: 113,
    text: "Cuba",
  },
  {
    id: 22,
    text: "Dinamarca",
  },
  {
    id: 169,
    text: "Djibouti, Yibuti",
  },
  {
    id: 103,
    text: "Ecuador",
  },
  {
    id: 23,
    text: "Egipto",
  },
  {
    id: 51,
    text: "El Salvador",
  },
  {
    id: 93,
    text: "Emiratos Árabes Unidos",
  },
  {
    id: 173,
    text: "Eritrea",
  },
  {
    id: 52,
    text: "Eslovaquia",
  },
  {
    id: 53,
    text: "Eslovenia",
  },
  {
    id: 28,
    text: "España",
  },
  {
    id: 55,
    text: "Estados Unidos",
  },
  {
    id: 68,
    text: "Estonia",
  },
  {
    id: 121,
    text: "Etiopía",
  },
  {
    id: 175,
    text: "Feroe, Islas",
  },
  {
    id: 90,
    text: "Filipinas",
  },
  {
    id: 63,
    text: "Finlandia",
  },
  {
    id: 176,
    text: "Fiyi",
  },
  {
    id: 64,
    text: "Francia",
  },
  {
    id: 180,
    text: "Gabón",
  },
  {
    id: 181,
    text: "Gambia",
  },
  {
    id: 21,
    text: "Georgia",
  },
  {
    id: 105,
    text: "Ghana",
  },
  {
    id: 143,
    text: "Gibraltar",
  },
  {
    id: 184,
    text: "Granada",
  },
  {
    id: 20,
    text: "Grecia",
  },
  {
    id: 94,
    text: "Groenlandia",
  },
  {
    id: 17,
    text: "Guadalupe",
  },
  {
    id: 185,
    text: "Guatemala",
  },
  {
    id: 186,
    text: "Guernsey",
  },
  {
    id: 187,
    text: "Guinea",
  },
  {
    id: 172,
    text: "Guinea Ecuatorial",
  },
  {
    id: 188,
    text: "Guinea-Bissau",
  },
  {
    id: 189,
    text: "Guyana",
  },
  {
    id: 16,
    text: "Haiti",
  },
  {
    id: 137,
    text: "Honduras",
  },
  {
    id: 73,
    text: "Hong Kong",
  },
  {
    id: 14,
    text: "Hungría",
  },
  {
    id: 25,
    text: "India",
  },
  {
    id: 74,
    text: "Indonesia",
  },
  {
    id: 140,
    text: "Irak",
  },
  {
    id: 26,
    text: "Irán",
  },
  {
    id: 27,
    text: "Irlanda",
  },
  {
    id: 215,
    text: "Isla Pitcairn",
  },
  {
    id: 83,
    text: "Islandia",
  },
  {
    id: 228,
    text: "Islas Salomón",
  },
  {
    id: 58,
    text: "Islas Turcas y Caicos",
  },
  {
    id: 154,
    text: "Islas Virgenes Británicas",
  },
  {
    id: 24,
    text: "Israel",
  },
  {
    id: 29,
    text: "Italia",
  },
  {
    id: 132,
    text: "Jamaica",
  },
  {
    id: 70,
    text: "Japón",
  },
  {
    id: 193,
    text: "Jersey",
  },
  {
    id: 75,
    text: "Jordania",
  },
  {
    id: 30,
    text: "Kazajstán",
  },
  {
    id: 97,
    text: "Kenia",
  },
  {
    id: 34,
    text: "Kirguistán",
  },
  {
    id: 195,
    text: "Kiribati",
  },
  {
    id: 37,
    text: "Kuwait",
  },
  {
    id: 196,
    text: "Laos",
  },
  {
    id: 197,
    text: "Lesotho",
  },
  {
    id: 38,
    text: "Letonia",
  },
  {
    id: 99,
    text: "Líbano",
  },
  {
    id: 198,
    text: "Liberia",
  },
  {
    id: 39,
    text: "Libia",
  },
  {
    id: 126,
    text: "Liechtenstein",
  },
  {
    id: 40,
    text: "Lituania",
  },
  {
    id: 41,
    text: "Luxemburgo",
  },
  {
    id: 85,
    text: "Macedonia",
  },
  {
    id: 134,
    text: "Madagascar",
  },
  {
    id: 76,
    text: "Malasia",
  },
  {
    id: 125,
    text: "Malawi",
  },
  {
    id: 200,
    text: "Maldivas",
  },
  {
    id: 133,
    text: "Malí",
  },
  {
    id: 86,
    text: "Malta",
  },
  {
    id: 131,
    text: "Man, Isla de",
  },
  {
    id: 104,
    text: "Marruecos",
  },
  {
    id: 201,
    text: "Martinica",
  },
  {
    id: 202,
    text: "Mauricio",
  },
  {
    id: 108,
    text: "Mauritania",
  },
  {
    id: 42,
    text: "México",
  },
  {
    id: 43,
    text: "Moldavia",
  },
  {
    id: 44,
    text: "Mónaco",
  },
  {
    id: 139,
    text: "Mongolia",
  },
  {
    id: 117,
    text: "Mozambique",
  },
  {
    id: 205,
    text: "Myanmar",
  },
  {
    id: 102,
    text: "Namibia",
  },
  {
    id: 206,
    text: "Nauru",
  },
  {
    id: 107,
    text: "Nepal",
  },
  {
    id: 209,
    text: "Nicaragua",
  },
  {
    id: 210,
    text: "Níger",
  },
  {
    id: 115,
    text: "Nigeria",
  },
  {
    id: 212,
    text: "Norfolk Island",
  },
  {
    id: 46,
    text: "Noruega",
  },
  {
    id: 208,
    text: "Nueva Caledonia",
  },
  {
    id: 45,
    text: "Nueva Zelanda",
  },
  {
    id: 213,
    text: "Omán",
  },
  {
    id: 19,
    text: "Países Bajos, Holanda",
  },
  {
    id: 87,
    text: "Pakistán",
  },
  {
    id: 124,
    text: "Panamá",
  },
  {
    id: 88,
    text: "Papúa-Nueva Guinea",
  },
  {
    id: 110,
    text: "Paraguay",
  },
  {
    id: 89,
    text: "Perú",
  },
  {
    id: 178,
    text: "Polinesia Francesa",
  },
  {
    id: 47,
    text: "Polonia",
  },
  {
    id: 48,
    text: "Portugal",
  },
  {
    id: 246,
    text: "Puerto Rico",
  },
  {
    id: 216,
    text: "Qatar",
  },
  {
    id: 13,
    text: "Reino Unido",
  },
  {
    id: 65,
    text: "República Checa",
  },
  {
    id: 138,
    text: "República Dominicana",
  },
  {
    id: 49,
    text: "Reunión",
  },
  {
    id: 217,
    text: "Ruanda",
  },
  {
    id: 72,
    text: "Rumanía",
  },
  {
    id: 50,
    text: "Rusia",
  },
  {
    id: 242,
    text: "Sáhara Occidental",
  },
  {
    id: 223,
    text: "Samoa",
  },
  {
    id: 219,
    text: "San Cristobal y Nevis",
  },
  {
    id: 224,
    text: "San Marino",
  },
  {
    id: 221,
    text: "San Pedro y Miquelón",
  },
  {
    id: 225,
    text: "San Tomé y Príncipe",
  },
  {
    id: 222,
    text: "San Vincente y Granadinas",
  },
  {
    id: 218,
    text: "Santa Elena",
  },
  {
    id: 220,
    text: "Santa Lucía",
  },
  {
    id: 135,
    text: "Senegal",
  },
  {
    id: 226,
    text: "Serbia y Montenegro",
  },
  {
    id: 109,
    text: "Seychelles",
  },
  {
    id: 227,
    text: "Sierra Leona",
  },
  {
    id: 77,
    text: "Singapur",
  },
  {
    id: 106,
    text: "Siria",
  },
  {
    id: 229,
    text: "Somalia",
  },
  {
    id: 120,
    text: "Sri Lanka",
  },
  {
    id: 141,
    text: "Sudáfrica",
  },
  {
    id: 232,
    text: "Sudán",
  },
  {
    id: 67,
    text: "Suecia",
  },
  {
    id: 66,
    text: "Suiza",
  },
  {
    id: 54,
    text: "Surinam",
  },
  {
    id: 234,
    text: "Swazilandia",
  },
  {
    id: 56,
    text: "Tadjikistan",
  },
  {
    id: 92,
    text: "Tailandia",
  },
  {
    id: 78,
    text: "Taiwan",
  },
  {
    id: 101,
    text: "Tanzania",
  },
  {
    id: 171,
    text: "Timor Oriental",
  },
  {
    id: 136,
    text: "Togo",
  },
  {
    id: 235,
    text: "Tokelau",
  },
  {
    id: 236,
    text: "Tonga",
  },
  {
    id: 237,
    text: "Trinidad y Tobago",
  },
  {
    id: 122,
    text: "Túnez",
  },
  {
    id: 57,
    text: "Turkmenistan",
  },
  {
    id: 59,
    text: "Turquía",
  },
  {
    id: 239,
    text: "Tuvalu",
  },
  {
    id: 62,
    text: "Ucrania",
  },
  {
    id: 60,
    text: "Uganda",
  },
  {
    id: 111,
    text: "Uruguay",
  },
  {
    id: 61,
    text: "Uzbekistán",
  },
  {
    id: 240,
    text: "Vanuatu",
  },
  {
    id: 95,
    text: "Venezuela",
  },
  {
    id: 15,
    text: "Vietnam",
  },
  {
    id: 241,
    text: "Wallis y Futuna",
  },
  {
    id: 243,
    text: "Yemen",
  },
  {
    id: 116,
    text: "Zambia",
  },
  {
    id: 96,
    text: "Zimbabwe",
  },
];

export {GPPEC_getCountry};