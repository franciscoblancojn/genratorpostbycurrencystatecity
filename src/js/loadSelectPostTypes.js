export default ({ GPPEC_postType, GPPEC_post }) => {
  const GPPEC_getPostType_Options = () => {
    return `
              <option disabled selected value="">Select</option>
              <option value="all">All</option>
              ${GPPEC_postTypeArray.map((postType) => {
                return `<option value="${postType}">${postType}</option>`;
              }).join("")}
          `;
  };

  const GPPEC_getPostType_Options_load = () => {
    GPPEC_postType.innerHTML = GPPEC_getPostType_Options();
    GPPEC_postType.value = "";
    GPPEC_getPost_Options_load();
  };

  const GPPEC_getPost = (postType) => {
    return GPPEC_postArray.filter((post) => post.post_type == postType);
  };

  const GPPEC_getPost_Options = () => {
    const POST = GPPEC_getPost(GPPEC_postType.value);
    if (POST.length == 0) {
      return `<option disabled selected value="">No hay Opciones</option>`;
    }
    return `
              <option disabled selected value="">Select</option>
              <option value="all">All</option>
              ${POST.map((post) => {
                return `<option value="${post.ID}">${post.post_title}</option>`;
              }).join("")}
          `;
  };
  const GPPEC_getPost_Options_load = () => {
    GPPEC_post.innerHTML = GPPEC_getPost_Options();
    GPPEC_post.value = "";
  };
  const GPPEC_loadSelectPost = () => {
    GPPEC_getPostType_Options_load();
    GPPEC_postType.addEventListener("change", GPPEC_getPost_Options_load);
  };
  GPPEC_loadSelectPost();
};
