export default (input) => {
  const autocomplete = new google.maps.places.Autocomplete(input);
  return autocomplete;
};
