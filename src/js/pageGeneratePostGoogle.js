import loadSelectPostTypes from "./loadSelectPostTypes.js";
import loadSubmitGeneratePost from "./loadSubmitGeneratePostGoogle.js";
import showRespondeGeneratePost from "./showRespondeGeneratePost.js";
import loadInputGoogle from "./loadInputGoogle.js";

const GPPEC_google = document.getElementById("GPPEC_google");

const GPPEC_postType = document.getElementById("GPPEC_postType");
const GPPEC_post = document.getElementById("GPPEC_post");

const GPPEC_connection = document.getElementById("GPPEC_connection");

const GPPEC_noDuplicates = document.getElementById("GPPEC_noDuplicates");

const GPPEC_responde = document.getElementById("GPPEC_responde");
const GPPEC_progress_number = document.getElementById("GPPEC_progress_number");
const GPPEC_progress_bar = document.getElementById("GPPEC_progress_bar");
const GPPEC_table_body = document.getElementById("GPPEC_table_body");

const GPPEC_formGeneratePost = document.getElementById(
  "GPPEC_formGeneratePost"
);


const GPPEC_load = () => {
  loadInputGoogle(GPPEC_google)
  const GPPEC_GeneratePost = showRespondeGeneratePost({
    GPPEC_URL,
    GPPEC_progress_bar,
    GPPEC_progress_number,
    GPPEC_responde,
    GPPEC_table_body,
    GPPEC_noDuplicates,
  });
  
  loadSelectPostTypes({ GPPEC_postType, GPPEC_post });
  loadSubmitGeneratePost({
    GPPEC_formGeneratePost,
    GPPEC_google,
    GPPEC_postType,
    GPPEC_post,
    GPPEC_GeneratePost,
    GPPEC_connection,
  });
};

window.addEventListener("load", GPPEC_load);
