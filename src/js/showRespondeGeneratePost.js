export default ({
  GPPEC_responde,
  GPPEC_progress_number,
  GPPEC_progress_bar,
  GPPEC_table_body,
  GPPEC_URL,
  GPPEC_noDuplicates,
}) => {
  const GPPEC_RequestGeneratePost = async (variation) => {
    try {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({
        variation,
        notDuplicate: GPPEC_noDuplicates.checked,
      });

      var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      const respond = await fetch(
        `${GPPEC_URL}src/routes/generatePost.php`,
        requestOptions
      );
      const result = await respond.json();
      return result;
    } catch (error) {
      return {
        state: 200,
        error,
        message: `${error}`,
      };
    }
  };

  const GPPEC_showResponde = ({ variation, result }) => {
    GPPEC_table_body.innerHTML = `${GPPEC_table_body.innerHTML}
      <tr class="${result.status == 200 ? "ok" : "error"}">
        <td>
          ${variation.postType}
        </td>
        <td>
          ${variation.post.ID}
        </td>
        <td>
          ${variation.post.post_title}
        </td>
        <td>
          ${variation.country.text}
        </td>
        <td>
          ${variation.state.text}
        </td>
        <td>
          ${variation.city.text}
        </td>
        <td>
          ${JSON.stringify(result)}
        </td>
      </tr>
    `;
  };

  const GPPEC_resetResponde = () => {
    GPPEC_table_body.innerHTML = "";
  };

  const GPPEC_GeneratePost = async (variations) => {
    GPPEC_responde.classList.add("active");
    GPPEC_responde.classList.add("load");
    GPPEC_resetResponde();
    GPPEC_progress_bar.max = variations.length;
    for (let i = 0; i < variations.length; i++) {
      const variation = variations[i];

      const result = await GPPEC_RequestGeneratePost(variation);

      GPPEC_progress_bar.value = i + 1;
      GPPEC_progress_number.innerHTML = `${
        ((i + 1) / variations.length) * 100
      }%`;
      GPPEC_showResponde({
        variation,
        result,
      });
      console.log({
        variation,
        result,
      });
    }
    GPPEC_responde.classList.remove("load");
  };
  return GPPEC_GeneratePost;
};
