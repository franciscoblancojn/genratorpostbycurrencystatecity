import { GPPEC_getCountry } from "./country.js";
import { GPPEC_geState } from "./state.js";
import { GPPEC_getCity } from "./city.js";

export default ({
  GPPEC_formGeneratePost,
  GPPEC_country,
  GPPEC_state,
  GPPEC_city,
  GPPEC_postType,
  GPPEC_post,
  GPPEC_GeneratePost,
  GPPEC_connection,
}) => {
  const ValidateProps = ({
    country,
    state,
    city,

    postType,
    post,
  }) => {
    if (country == "") {
      alert("Country requered");
      return false;
    }
    if (country != "all") {
      if (state == "") {
        alert("State requered");
        return false;
      }
      if (state != "all") {
        if (city == "") {
          alert("City requered");
          return false;
        }
      }
    }
    if (postType == "") {
      alert("Post Type requered");
      return false;
    }
    if (postType != "all") {
      if (post == "") {
        alert("Post requered");
        return false;
      }
    }

    return true;
  };

  const SubmitGeneratePost = () => {
    const props = {
      country: GPPEC_country.value,
      state: GPPEC_state.value,
      city: GPPEC_city.value,

      postType: GPPEC_postType.value,
      post: GPPEC_post.value,
    };
    if (ValidateProps(props)) {
      const variations = [];

      var countrysSelected = [];
      var statesSelected = [];
      var citysSelected = [];

      var postTypesSelected = [];
      var postsSelected = [];

      if (props.country == "all") {
        countrysSelected = GPPEC_getCountry();
        statesSelected = GPPEC_geState("", true);
        citysSelected = GPPEC_getCity("", true);
      } else {
        countrysSelected = GPPEC_getCountry().filter(
          (e) => e.id == props.country
        );
        if (props.state == "all") {
          statesSelected = GPPEC_geState("", true);
          citysSelected = GPPEC_getCity("", true);
        } else {
          statesSelected = GPPEC_geState(props.country).filter(
            (e) => e.id == props.state
          );
          if (props.city == "all") {
            citysSelected = GPPEC_getCity("", true);
          } else {
            citysSelected = GPPEC_getCity(props.state).filter(
              (e) => e.id == props.city
            );
          }
        }
      }

      if (props.postType == "all") {
        postTypesSelected = GPPEC_postTypeArray;
        postsSelected = GPPEC_postArray;
      } else {
        postTypesSelected = [props.postType];
        if (props.post == "all") {
          postsSelected = GPPEC_postArray.filter(
            (post) => post.post_type == props.postType
          );
        } else {
          postsSelected = GPPEC_postArray.filter(
            (post) => post.ID == props.post
          );
        }
      }
      for (let x = 0; x < postTypesSelected.length; x++) {
        const postType = postTypesSelected[x];
        const postsSelectedList = postsSelected.filter(
          (post) => post.post_type == postType
        );
        for (let y = 0; y < postsSelectedList.length; y++) {
          const post = postsSelectedList[y];
          for (let i = 0; i < countrysSelected.length; i++) {
            const country = countrysSelected[i];
            const statesSelectedList = statesSelected.filter(
              (e) => e.id_country == country.id
            );
            variations.push({
              postType,
              post,
              country,
              extraTitle: `${GPPEC_connection.value} ${country.text}`,
              state: {
                id: -1,
                text: "null",
              },
              city: {
                id: -1,
                text: "null",
              },
              address: "null",
            });
            for (let j = 0; j < statesSelectedList.length; j++) {
              const state = statesSelectedList[j];
              const citysSelectedList = citysSelected.filter(
                (e) => e.id_state == state.id
              );
              variations.push({
                postType,
                post,
                country,
                state,
                extraTitle: `${GPPEC_connection.value} ${state.text}`,
                city: {
                  id: -1,
                  text: "null",
                },
                address: "null",
              });
              for (let k = 0; k < citysSelectedList.length; k++) {
                const city = citysSelectedList[k];
                variations.push({
                  postType,
                  post,
                  country,
                  state,
                  city,
                  extraTitle: `${GPPEC_connection.value} ${city.text}`,
                  address: "null",
                });
              }
            }
          }
        }
      }
      GPPEC_GeneratePost(variations);
    }
  };

  GPPEC_formGeneratePost.onsubmit = (e) => {
    e.preventDefault();
    SubmitGeneratePost();
  };
};
