export default ({
  GPPEC_formGeneratePost,
  GPPEC_google,
  GPPEC_postType,
  GPPEC_post,
  GPPEC_GeneratePost,
  GPPEC_connection,
}) => {
  const ValidateProps = ({ google, postType, post }) => {
    if (google == "") {
      alert("Google Address requered");
      return false;
    }
    if (postType == "") {
      alert("Post Type requered");
      return false;
    }
    if (postType != "all") {
      if (post == "") {
        alert("Post requered");
        return false;
      }
    }

    return true;
  };

  const SubmitGeneratePost = () => {
    const props = {
      google: GPPEC_google.value,

      postType: GPPEC_postType.value,
      post: GPPEC_post.value,
    };
    if (ValidateProps(props)) {
      const variations = [];

      const googleSelected = [props.google];

      var postTypesSelected = [];
      var postsSelected = [];

      if (props.postType == "all") {
        postTypesSelected = GPPEC_postTypeArray;
        postsSelected = GPPEC_postArray;
      } else {
        postTypesSelected = [props.postType];
        if (props.post == "all") {
          postsSelected = GPPEC_postArray.filter(
            (post) => post.post_type == props.postType
          );
        } else {
          postsSelected = GPPEC_postArray.filter(
            (post) => post.ID == props.post
          );
        }
      }
      for (let x = 0; x < postTypesSelected.length; x++) {
        const postType = postTypesSelected[x];
        const postsSelectedList = postsSelected.filter(
          (post) => post.post_type == postType
        );
        for (let y = 0; y < postsSelectedList.length; y++) {
          const post = postsSelectedList[y];
          for (let i = 0; i < googleSelected.length; i++) {
            const google = googleSelected[i];
            variations.push({
              postType,
              post,
              address: google,
              extraTitle: `${GPPEC_connection.value} ${google}`,
              country: {
                id: -1,
                text: "null",
              },
              state: {
                id: -1,
                text: "null",
              },
              city: {
                id: -1,
                text: "null",
              },
            });
          }
        }
      }
      GPPEC_GeneratePost(variations);
    }
  };

  GPPEC_formGeneratePost.onsubmit = (e) => {
    e.preventDefault();
    SubmitGeneratePost();
  };
};
