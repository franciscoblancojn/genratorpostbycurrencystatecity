import { GPPEC_getCountry } from "./country.js";
import { GPPEC_geState } from "./state.js";
import { GPPEC_getCity } from "./city.js";

export default ({ GPPEC_country, GPPEC_state, GPPEC_city }) => {
  const GPPEC_getCountry_Options = () => {
    return `
              <option disabled selected value="">Select</option>
              <option value="all">All</option>
              ${GPPEC_getCountry()
                .map((country) => {
                  return `<option value="${country.id}">${country.text}</option>`;
                })
                .join("")}
          `;
  };
  const GPPEC_getCountry_Options_load = () => {
    GPPEC_country.innerHTML = GPPEC_getCountry_Options();
    GPPEC_country.value = "";
    GPPEC_getState_Options_load();
  };

  const GPPEC_getState_Options = () => {
    const STATES = GPPEC_geState(GPPEC_country.value);
    if (STATES.length == 0) {
      return `<option disabled selected value="">No hay Opciones</option>`;
    }
    return `
              <option disabled selected value="">Select</option>
              <option value="all">All</option>
                ${STATES.map((state) => {
                  return `<option value="${state.id}">${state.text}</option>`;
                }).join("")}
            `;
  };
  const GPPEC_getState_Options_load = () => {
    GPPEC_state.innerHTML = GPPEC_getState_Options();
    GPPEC_state.value = "";
    GPPEC_getCity_Options_load();
  };

  const GPPEC_getCity_Options = () => {
    const CITYS = GPPEC_getCity(GPPEC_state.value);
    if (CITYS.length == 0) {
      return `<option disabled selected value="">No hay Opciones</option>`;
    }
    return `
                <option disabled selected value="">Select</option>
                <option value="all">All</option>
                ${CITYS.map((city) => {
                  return `<option value="${city.id}">${city.text}</option>`;
                }).join("")}
            `;
  };
  const GPPEC_getCity_Options_load = () => {
    GPPEC_city.innerHTML = GPPEC_getCity_Options();
    GPPEC_city.value = "";
  };

  const GPPEC_loadSelect = () => {
    GPPEC_getCountry_Options_load();
    GPPEC_country.addEventListener("change", GPPEC_getState_Options_load);
    GPPEC_state.addEventListener("change", GPPEC_getCity_Options_load);
  };

  GPPEC_loadSelect();
};
