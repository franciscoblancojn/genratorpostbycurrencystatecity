<?php

require_once GPPEC_PATH . "src/function/_index.php";
require_once GPPEC_PATH . "src/validators/_index.php";
require_once GPPEC_PATH . "src/hook/_index.php";
require_once GPPEC_PATH . "src/includes/_index.php";
require_once GPPEC_PATH . "src/shortcode/_index.php";

require_once GPPEC_PATH . "src/js/_index.php";
require_once GPPEC_PATH . "src/sass/_index.php";
require_once GPPEC_PATH . "src/css/_index.php";
require_once GPPEC_PATH . "src/img/_index.php";

require_once GPPEC_PATH . "src/pages/_index.php";
require_once GPPEC_PATH . "src/log/_index.php";

require_once GPPEC_PATH . "src/api/_index.php";
require_once GPPEC_PATH . "src/payment/_index.php";