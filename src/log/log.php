<?php
if(GPPEC_LOG){
    function add_GPPEC_LOG_option_page($admin_bar)
    {
        global $pagenow;
        $admin_bar->add_menu(
            array(
                'id' => 'GPPEC_LOG',
                'title' => 'GPPEC_LOG',
                'href' => get_site_url().'/wp-admin/options-general.php?page=GPPEC_LOG'
            )
        );
    }

    function GPPEC_LOG_option_page()
    {
        add_options_page(
            'Log OAcquiring Tech',
            'Log OAcquiring Tech',
            'manage_options',
            'GPPEC_LOG',
            'GPPEC_LOG_page'
        );
    }

    function GPPEC_LOG_page()
    {
        $log = GPPEC_get_optionPage("GPPEC_LOG");
        $log = array_reverse($log);
        ?>
        <script>
            const log = <?=json_encode($log)?>;
        </script>
        <h1>
            Log de OAcquiring Tech
        </h1>
        <pre><?php var_dump($log);?></pre>
        <?php
    }
    add_action('admin_bar_menu', 'add_GPPEC_LOG_option_page', 100);

    add_action('admin_menu', 'GPPEC_LOG_option_page');

}

function addGPPEC_LOG($newLog)
{
    if(!GPPEC_LOG){
        return;
    }
    GPPEC_put_optionPage("GPPEC_LOG",$newLog);
}