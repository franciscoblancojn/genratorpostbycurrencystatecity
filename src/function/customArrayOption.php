<?php

function GPPEC_get_optionPage($id = "GPPEC_optionPage")
{
    $GPPEC_optionPage = get_option($id);
    if($GPPEC_optionPage === false || $GPPEC_optionPage == null || $GPPEC_optionPage == ""){
        $GPPEC_optionPage = "[]";
    }
    $GPPEC_optionPage = json_decode($GPPEC_optionPage,true);
    return $GPPEC_optionPage;
}

function GPPEC_put_optionPage($id,$newItem)
{
    $GPPEC_optionPage = GPPEC_get_optionPage($id);
    $GPPEC_optionPage[] = array(
        "date" => date("c"),
        "data" => $newItem,
    );
    update_option($id,json_encode($GPPEC_optionPage));
}