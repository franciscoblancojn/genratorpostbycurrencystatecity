<?php

function GPPEC_errorApi($messaje, $status = 400, $exit = true)
{
    echo json_encode(array(
        "status"    => $status,
        "type"      => "error",
        "messaje"   => $messaje,
    ));
    if($exit){
        exit;
    }
}