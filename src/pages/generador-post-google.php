<?php
function GPPEC_google_create_menu() {

	//create new top-level menu
	add_submenu_page("GPPEC_generate_post",'Generador Post Google', 'Google', 'administrator', "GPPEC_generate_post_google", 'GPPEC_generate_post_google_page'  );

	//call register settings function
	add_action( 'admin_init', 'register_GPPEC_google_settings' );
}
add_action('admin_menu', 'GPPEC_google_create_menu');


function register_GPPEC_google_settings() {
	//register our settings
	register_setting( 'GPPEC-google-settings-group', 'new_option_name' );
	register_setting( 'GPPEC-google-settings-group', 'some_other_option' );
	register_setting( 'GPPEC-google-settings-group', 'option_etc' );
}

function GPPEC_generate_post_google_page() {
	$allPost = get_posts([
		'post_type' => 'any',
		'post_status' => 'publish',
		'numberposts' => -1,
		'meta_query' => array(
			array(
			 'key' => 'GPPEC_post',
			 'compare' => 'NOT EXISTS'
			),
		)
	]);

	$allPostType = [];

	for ($i=0; $i < count($allPost); $i++) { 
		$allPostType[$allPost[$i]->post_type] = $allPost[$i]->post_type;
	}
	$allPostType = array_values($allPostType);

    if($_POST){
        update_option("GPPEC_google_apiKey",$_POST["GPPEC_google_apiKey"]);
    }
    $GPPEC_google_apiKey = get_option("GPPEC_google_apiKey");
    ?>
	<link rel="stylesheet" href="<?=GPPEC_URL?>src/css/pageGeneratePost.css?v=<?=GPPEC_get_version()?>">
    <div class="wrap">
        <h1>
            Generador de Post por Google
        </h1>
    </div>

	<h3>
		Generar Posts
	</h3>
	<h4>
		Use [GPPEC_getCustomField customField="customField"] for show custom field
		<br>
		For show GPPEC_extraTitle use [GPPEC_getCustomField customField="GPPEC_extraTitle"]
	</h4>
	<form method="post" id="GPPEC_formSaveGoogleApiKey" class="GPPEC_formGeneratePost">
		<label>
            Google Map Api Key
			<br>
            <input 
                name="GPPEC_google_apiKey" 
                id="GPPEC_google_apiKey" 
                placeholder="Google Api Key" 
                type="text" 
                value="<?=$GPPEC_google_apiKey?>"
            />
		</label>
		<button class="button action">Save</button>
    </form>
    <br>
	<form method="post" id="GPPEC_formGeneratePost" class="GPPEC_formGeneratePost">
		<label>
			Google Address
			<br>
			<input name="GPPEC_google" id="GPPEC_google" placeholder="Google Address" type="text"/>
		</label>
		<label>
			Post Type
			<br>
			<select name="GPPEC_postType" id="GPPEC_postType" placeholder="PostType"></select>
		</label>
		<label>
			Post
			<br>
			<select name="GPPEC_post" id="GPPEC_post" placeholder="Post"></select>
		</label>
		<label>
			Connection
			<br>
			<input name="GPPEC_connection" id="GPPEC_connection" placeholder="Connection" type="text" value="en"/>
		</label>
		<label>
			No Duplicates
			<input name="GPPEC_noDuplicates" id="GPPEC_noDuplicates" type="checkbox" checked/>
		</label>

		<button class="button action">Generate</button>
	</form>
	<div id="GPPEC_responde" class="responde">
		<div id="GPPEC_progress">
			Percentage
			<span id="GPPEC_progress_number"></span>
		</div>
		<progress id="GPPEC_progress_bar" max="100" value="0"></progress>
		<table id="GPPEC_table">
			<thead>
				<tr>
					<th>
						Post Type
					</th>
					<th>
						Post ID
					</th>
					<th>
						Post Name
					</th>
					<th>
						Country
					</th>
					<th>
						State
					</th>
					<th>
						City
					</th>
					<th>
						Result
					</th>
				</tr>
			</thead>
			<tbody id="GPPEC_table_body">

			</tbody>
		</table>
	</div>
	<script>
		const GPPEC_LOG = `<?=GPPEC_LOG?>`
		const GPPEC_PATH = `<?=GPPEC_PATH?>`
		const GPPEC_URL = `<?=GPPEC_URL?>`

		const GPPEC_postArray = <?=json_encode($allPost);?>;
		const GPPEC_postTypeArray = <?=json_encode($allPostType);?>;
	</script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?=$GPPEC_google_apiKey?>&libraries=places"></script>
	<script src="<?=GPPEC_URL?>src/js/pageGeneratePostGoogle.js?v=<?=GPPEC_get_version()?>" type="module"></script>
    <?php 
}