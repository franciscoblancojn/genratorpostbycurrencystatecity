<?php
function GPPEC_create_menu() {

	//create new top-level menu
	add_menu_page('Generador Post Settings', 'Generador Post', 'administrator', "GPPEC_generate_post", 'GPPEC_settings_page'  );

	//call register settings function
	add_action( 'admin_init', 'register_GPPEC_settings' );
}
add_action('admin_menu', 'GPPEC_create_menu');


function register_GPPEC_settings() {
	//register our settings
	register_setting( 'GPPEC-settings-group', 'new_option_name' );
	register_setting( 'GPPEC-settings-group', 'some_other_option' );
	register_setting( 'GPPEC-settings-group', 'option_etc' );
}

function GPPEC_settings_page() {
	$allPost = get_posts([
		'post_type' => 'any',
		'post_status' => 'publish',
		'numberposts' => -1,
		'meta_query' => array(
			array(
			 'key' => 'GPPEC_post',
			 'compare' => 'NOT EXISTS'
			),
		)
	]);

	$allPostType = [];

	for ($i=0; $i < count($allPost); $i++) { 
		$allPostType[$allPost[$i]->post_type] = $allPost[$i]->post_type;
	}
	$allPostType = array_values($allPostType);

    ?>
	<link rel="stylesheet" href="<?=GPPEC_URL?>src/css/pageGeneratePost.css?v=<?=GPPEC_get_version()?>">
    <div class="wrap">
        <h1>
            Generador de Post por Paices, Estados y Ciudades
        </h1>
    </div>

	<h3>
		Generar Posts
	</h3>
	<h4>
		Use [GPPEC_getCustomField customField="customField"] for show custom field
		<br>
		For show GPPEC_extraTitle use [GPPEC_getCustomField customField="GPPEC_extraTitle"]
	</h4>
	<form method="post" id="GPPEC_formGeneratePost" class="GPPEC_formGeneratePost">
		<label>
			Country
			<br>
			<select name="GPPEC_country" id="GPPEC_country" placeholder="Country"></select>
		</label>
		<label>
			State
			<br>
			<select name="GPPEC_state" id="GPPEC_state" placeholder="State"></select>
		</label>
		<label>
			City
			<br>
			<select name="GPPEC_city" id="GPPEC_city" placeholder="City"></select>
		</label>

		<label>
			Post Type
			<br>
			<select name="GPPEC_postType" id="GPPEC_postType" placeholder="PostType"></select>
		</label>
		<label>
			Post
			<br>
			<select name="GPPEC_post" id="GPPEC_post" placeholder="Post"></select>
		</label>
		<label>
			Connection
			<br>
			<input name="GPPEC_connection" id="GPPEC_connection" placeholder="Connection" type="text" value="en"/>
		</label>
		<label>
			No Duplicates
			<input name="GPPEC_noDuplicates" id="GPPEC_noDuplicates" type="checkbox" checked/>
		</label>

		<button class="button action">Generate</button>
	</form>
	<div id="GPPEC_responde" class="responde">
		<div id="GPPEC_progress">
			Percentage
			<span id="GPPEC_progress_number"></span>
		</div>
		<progress id="GPPEC_progress_bar" max="100" value="0"></progress>
		<table id="GPPEC_table">
			<thead>
				<tr>
					<th>
						Post Type
					</th>
					<th>
						Post ID
					</th>
					<th>
						Post Name
					</th>
					<th>
						Country
					</th>
					<th>
						State
					</th>
					<th>
						City
					</th>
					<th>
						Result
					</th>
				</tr>
			</thead>
			<tbody id="GPPEC_table_body">

			</tbody>
		</table>
	</div>
	<script>
		const GPPEC_LOG = `<?=GPPEC_LOG?>`
		const GPPEC_PATH = `<?=GPPEC_PATH?>`
		const GPPEC_URL = `<?=GPPEC_URL?>`

		const GPPEC_postArray = <?=json_encode($allPost);?>;
		const GPPEC_postTypeArray = <?=json_encode($allPostType);?>;
	</script>
	<script src="<?=GPPEC_URL?>src/js/pageGeneratePost.js?v=<?=GPPEC_get_version()?>" type="module"></script>
    <?php 
}